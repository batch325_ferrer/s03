-- [SECTION] Insert records (CREATE)

-- INSERT INTO table_name (table_column) VALUES (column_value)

INSERT INTO artists(name) VALUES ('Rivermaya');
INSERT INTO artists(name) VALUES ('Psy');
INSERT INTO artists(name) VALUES ('Weird AL Yankovic');

SELECT * FROM artists;

DESCRIBE artists;

INSERT INTO albums(album_title,date_released,artist_id) VALUES ('PSY 6', "2012-1-1", 2);

INSERT INTO albums(album_title,date_released,artist_id) VALUES ('Trip', "1996-1-1", 1);


INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ('Gangnum Style', 253, 'K-POP', 1);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ('Kundiman', 234, 'OPM', 2);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ('Kisapmata', 259, 'OPM', 2);

ALTER TABLE [tbl_name] MODIFY COLUMN [col_name_1] [DATA_TYPE]

ALTER TABLE songs MODIFY COLUMN song_length TIME;


MariaDB [music_db]> describe songs;
+-------------+-------------+------+-----+---------+----------------+
| Field       | Type        | Null | Key | Default | Extra          |
+-------------+-------------+------+-----+---------+----------------+
| id          | int(11)     | NO   | PRI | NULL    | auto_increment |
| song_title  | varchar(50) | NO   |     | NULL    |                |
| song_length | time        | YES  |     | NULL    |                |
| song_genre  | varchar(50) | NO   |     | NULL    |                |
| album_id    | int(11)     | NO   | MUL | NULL    |                |
+-------------+-------------+------+-----+---------+----------------+
5 rows in set (0.005 sec)

MariaDB [music_db]> select * from songs;
+----+---------------+-------------+------------+----------+
| id | song_title    | song_length | song_genre | album_id |
+----+---------------+-------------+------------+----------+
|  2 | Gangnum Style | 00:02:53    | K-POP      |        1 |
|  3 | Kundiman      | 00:02:34    | OPM        |        2 |
+----+---------------+-------------+------------+----------+
2 rows in set (0.000 sec)

MariaDB [music_db]>



--- [Section] Selecting Records (Retrieve)

-- Retrieve all the records in a specific table
-- SELECT * FROM table_name;

SELECT * FROM songs;

SELECT * FROM albums;


-- Retrieve allthe records but not all the colum

SELECT song_title,song_length FROM songs;


-- Retrieve records given a specific conditions

SELECT * FROM songs WHERE song_genre = "OPM";


-- AND and OR keyword
SELECT * FROM songs WHERE song_length > 240 AND genre = "OPM";


ALTER TABLE songs RENAME COLUMN song_title TO title;


--[section] updating records

-- update table_name SET column_name =value where condition

UPDATE songs SET length = 240;

ALTER TABLE songs RENAME COLUMN song_title TO song_name;


UPDATE songs SET length = 259 WHERE id = 3;



--[section] delete records
--DELETE FROM table_name WHERE condition


DELETE FROM songs WHERE genre ="OPM" AND length >240;

UPDATE albums SET ID =100 WHERE album_title = "PSY 6"


DELETE FROM albums WHERE album_title = "PSY 6";

UPDATE songs SET id=1 WHERE id =2;